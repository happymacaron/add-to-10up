classdef Player
	properties
		left = 1;
		right = 1;
	end
	methods
		function output = won(obj)
			if obj.left == 0 && obj.right == 0
				output = 'both';
			elseif obj.left == 0
				output = 'left';
			elseif obj.right == 0
				output = 'right';
			else
				output = 'none';
			end
		end
	end
end