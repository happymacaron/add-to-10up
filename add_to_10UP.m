function add_to_10UP()
	clear all
	gameMatrix = GameMatrix();

	% Startup promt
	fprintf('\nWelcome to the game of finger adding to 10\n\n')
	fprintf('Before we start, do you want to know the rule of the game?\n')
	helpx = input('Input Y/N:    ','s');
	switch(helpx)
		case 'Y'
			helpText = {'\nThe two fingers of yours and CPUs represent numbers. \n\n',
						'Each side can add one of the other side''s number to one of his own numbers. \n\n',
						'The goal of the game is to make the two numbers of your side become 10 first. \n\n',
						'You and CPU will take moves in sequnce, and you will take the first move. \n\n',
						'When one finger reaches 10, it turns 0 automatically and will be unable to add or to be added. \n\n',
						'The program will automatically take moves if there are no other possibilities. \n\n',
						'Good Luck :-)'};
			for n = 1:length(helpText)
				fprintf(helpText{n});
				pause();
			end

		case 'N'
			fprintf('\nOK~ Press to start\n')
			pause();
		otherwise
			fprintf('\nNot Available \nGame starts after press')
			pause();
	end
	fprintf('\n\nGame Starts')
	fprintf('\n\nDifficulty Select')
	fprintf('\nWhich mode do you want to try\n')

	gameMode = input('select: easy/normal/hard/crazy/impossible:  ','s');
	difficulty = 0;
	switch(gameMode)
		case 'easy'
			difficulty = 0;
		case 'normal'
			difficulty = 0.3;
		case 'hard'
			difficulty = 0.7;
		case 'crazy'
			difficulty = 1;
		case 'impossible'
			difficulty = 10;
		otherwise
			fprintf('Not Available')
			fprintf('\n')
			fprintf('\n')
			fprintf('Automatically set difficulty as "normal"')
			fprintf('\n')
			gameMode = 'normal';
	end
	fprintf('\n')
	fprintf('Selected Mode: %s', gameMode)
	pause();

	currentMatrix = gameMatrix.matrix();
	% I switched the ordering of the rows.
	Computer_Left_Right_Finger = currentMatrix(1,:)
	Your_Left_Right_Finger = currentMatrix(2,:)

	fprintf('To simplify, we use a matrix to illustrate\n')

	Start = gameMatrix.matrix()

	fprintf('let''s play :-) \n\n')


	% The gaming loop
	while gameOn(gameMatrix)
		playerMove(gameMatrix)
		disp('After your move:  ');
		disp(gameMatrix.matrix());
		if ~gameOn(gameMatrix)
			break
		end
		computerMove(gameMatrix, difficulty)        
		NOW = gameMatrix.matrix()
	end

	if gameMatrix.player.won() == 'both'
		fprintf('You win')
		fprintf('\n')
	elseif gameMatrix.computer.won() == 'both'
		fprintf('CPU wins')
		fprintf('\n')
	end
end


function output = gameOn(gameMatrix)
	player = gameMatrix.player;
	computer = gameMatrix.computer;
	output = ~(strcmp(player.won(), 'both') || strcmp(computer.won(), 'both'));
end


%
%
% @brief      pickLeftRight
%
% @param      possibilities  a 2 by 1 matrix that adds up to 1.
%
% @return     left or right
%
function output = pickLeftRight(possibilities)
	output = randsample({'left','right'}, 1, true, possibilities);
end


function output = theOtherHand(hand)
	switch hand
		case 'left'
			output = 'right';
		case 'right'
			output = 'left';
		otherwise
			return
	end
end


function playerMove(gameMatrix)
	player = gameMatrix.player;
	computer_hand = 'none';
	player_hand = 'none';
	if player.left == 0
		player_hand = 'right';
	elseif player.right == 0
		player_hand = 'left';
	else
		while true
			player_hand = input('Which hand do you want to move? ','s');
			if strcmp(player_hand, 'left') || strcmp(player_hand, 'right')
				break
			else
				fprintf('Not Available\n')
			end
		end
	end
	while true
		computer_hand = input('Which hand do you want to add? ','s');
		if strcmp(computer_hand, 'left') || strcmp(computer_hand, 'right')
			break
		else
				fprintf('Not Available\n')
		end
	end

	gameMatrix.add('player', player_hand, 'computer', computer_hand)
end


function computerMove(gameMatrix, difficulty)
	player = gameMatrix.player;
	computer = gameMatrix.computer;
	computer_hand = 'none';
	player_hand = 'none';

	if difficulty == 0
		if ~strcmp(player.won(), 'none') && strcmp(computer.won(), 'none')
			player_hand = theOtherHand(player.won());
			if computer.left + player.(player_hand) == 10
				computer_hand = 'left';
			elseif computer.right + player.(player_hand) == 10
				computer_hand = 'right';
			else
				computer_hand = pickLeftRight([0.5, 0.5]);
			end

		elseif strcmp(player.won(), 'none') && ~strcmp(computer.won(), 'none')
			computer_hand = theOtherHand(computer.won());
			if computer.(computer_hand) + player.left == 10
				player_hand = 'left';
			elseif computer.(computer_hand) + player.right == 10
				player_hand = 'right';
			else
				player_hand = pickLeftRight([0.5, 0.5]);
			end

		elseif ~strcmp(player.won(), 'none') && ~strcmp(computer.won(), 'none')
			computer_hand = theOtherHand(computer.won());
			player_hand = theOtherHand(player.won());
		else
			for try_computer_hand = {'left', 'right'}
				try_computer_hand = char(try_computer_hand);
				for try_player_hand = {'left', 'right'}
					try_player_hand = char(try_player_hand);
					if player.(try_player_hand) + computer.(try_computer_hand) == 10
						computer_hand = try_computer_hand;
						player_hand = try_player_hand;
						break
					end
				end
			end
			if strcmp(computer_hand, 'none') && strcmp(player_hand, 'none')
				player_hand = pickLeftRight([0.5, 0.5]);
				computer_hand = pickLeftRight([0.5, 0.5]);
			end
		end
	end
	computer_hand = char(computer_hand);
	player_hand = char(player_hand);
	gameMatrix.add('computer', computer_hand, 'player', player_hand)
end
