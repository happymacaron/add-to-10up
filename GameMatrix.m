classdef GameMatrix < handle
	properties
		player = Player();
		computer = Player();
	end
	methods
	% See http://www.mathworks.com/help/matlab/matlab_oop/specifying-methods-and-functions.html
		function add(obj, addTo, selfLR, addFrom, othersLR)
			switch addTo
				case 'player'
					obj.player.(selfLR) = ...
						mod(obj.player.(selfLR) + obj.computer.(othersLR), 10);
					fprintf('Your %s adds computer %s\n', selfLR, othersLR);
				case 'computer'
					obj.computer.(selfLR) = ...
						mod(obj.player.(othersLR) + obj.computer.(selfLR), 10);
					fprintf('Computer %s adds your %s\n', selfLR, othersLR);
			end
		end
		function output = matrix(obj)
			output = [obj.computer.left, obj.computer.right;
					  obj.player.left, obj.player.right];
		end
	end
end